/*
Problem:
Given an array of integers, return the largest range, inclusive, of integers that are all included in the array.
For example, given the array [9, 6, 1, 3, 8, 10, 12, 11], return (8, 12) since 8, 9, 10, 11, and 12 are all in the array.

Comments:
-Would be better to use vector, but I'm pretending using array is necessary.
-I'm supposing if two or more ranges are the largest, it can return any of them.
-I'm supposing an empty array should make the function throw.
*/

#include<array>
#include<vector>
#include<iostream>

template<typename Number>
class Range {
public:
    Range() = default;
    Range(Number x, Number y) : lower_bound(x), upper_bound(y) {
        if (x > y) {
            lower_bound = y;
            upper_bound = x;
        }
    }
    bool operator==(const Range<Number>& other) const {
        return lower_bound == other.lower_bound &&
               upper_bound == other.upper_bound;
    }
    template<typename T>
    friend std::ostream& operator<<(std::ostream&, const Range<T>&);
    bool contains(Number n) const {
        return lower_bound <= n && n <= upper_bound;
    }
    bool neighbors_lower(Number n) const {
        return n == lower_bound - 1;
    }
    bool neighbors_upper(Number n) const {
        return n == upper_bound + 1;
    }
    void extend_left() {
        --lower_bound;
    }
    void extend_right() {
        ++upper_bound;
    }
    bool neighbors_lower(const Range<Number>& other) const {
        return other.upper_bound + 1 == lower_bound;
    }
    bool neighbors_upper(const Range<Number>& other) const {
        return other.lower_bound - 1 == upper_bound;
    }
    Range<Number> join_left(Range<Number>& other) const {
        return Range<Number>(other.lower_bound, upper_bound);
    }
    Range<Number> join_right(Range<Number>& other) const {
        return Range<Number>(lower_bound, other.upper_bound);
    }
    Number size() const {
        return upper_bound - lower_bound;
    }
private:
    Number lower_bound;
    Number upper_bound;
};

template<typename Number>
std::ostream& operator<<(std::ostream& os, const Range<Number>& r) {
    os << '(' << r.lower_bound << ", " << r.upper_bound << ')';
    return os;
}

class DisjointIntRangeCollection {
public:
    friend std::ostream& operator<<(std::ostream&, const DisjointIntRangeCollection&);
    void include(int n) {
        if (any_contains(n)) return;
        int extended_left_index = -1;
        int extended_right_index = -1;
        for (size_t i = 0; i < collection.size(); ++i) {
            Range<int>& range = collection[i];
            if (range.neighbors_lower(n)) {
                range.extend_left();
                extended_left_index = i;
                break;
            }
            if (range.neighbors_upper(n)) {
                range.extend_right();
                extended_right_index = i;
                break;
            }
        }
        if (extended_left_index >= 0) {
            Range<int> extended_range = collection[extended_left_index];
            for (size_t i = 0; i < collection.size(); ++i) {
                Range<int> range = collection[i];
                if (range.neighbors_upper(extended_range)) {
                    Range<int> joined_range = range.join_right(extended_range);
                    collection[i] = joined_range;
                    collection.erase(collection.begin() + extended_left_index);
                }
            }
        } else if (extended_right_index >= 0) {
            Range<int> extended_range = collection[extended_right_index];
            for (size_t i = 0; i < collection.size(); ++i) {
                Range<int> range = collection[i];
                if (range.neighbors_lower(extended_range)) {
                    Range<int> joined_range = range.join_left(extended_range);
                    collection[i] = joined_range;
                    collection.erase(collection.begin() + extended_right_index);
                }
            }
        } else {
            collection.push_back(Range(n,n));
        }
    }
    bool any_contains(int n) const {
        for (Range<int> range : collection) {
            if (range.contains(n)) return true;
        }
        return false;
    }
    Range<int> get_largest() const {
        if (collection.size() == 0) throw std::runtime_error("empty collection"); //TODO: more specific error
        Range<int> largest = collection[0];
        for (Range<int> range : collection) {
            if (range.size() > largest.size()) {
                largest = range;
            }
        }
        return largest;
    }
private:
    std::vector<Range<int>> collection;
};

std::ostream& operator<<(std::ostream& os, const DisjointIntRangeCollection& c) {
    for (Range<int> range : c.collection) {
        os << range << ", ";
    }
    return os;
}

template<size_t size>
Range<int> get_largest_range_included_in(std::array<int, size> array) {
    if (size == 0) throw std::runtime_error("array has size 0"); //TODO: more specific error type
    DisjointIntRangeCollection ranges;
    for (int current_number : array) {
        ranges.include(current_number);
    }
    Range<int> largest = ranges.get_largest();
    return largest;
}

template<size_t size>
bool test(std::array<int, size> in, Range<int> out) {
    return get_largest_range_included_in(in) == out;
}

int main() {
    std::array array1 = {9, 6, 1, 3, 8, 10, 12, 11};
    if (!test(array1, Range<int>(8, 12))) return 1;
    std::array array2 = {1, 4, 2, 7, 4, 6, 3};
    if (!test(array2, Range<int>(1, 4))) return 2;
    std::array array3 = {2, 4, 7, 23, 18, 20};
    if (!test(array3, Range<int>(2, 2))) return 3;
    std::array array4 = {17};
    if (!test(array4, Range<int>(17, 17))) return 4;
    std::array<int, 0> array5 = {};
    try {
        get_largest_range_included_in(array5);
        return 5;
    } catch (...) {} //TODO: more specific catch
    return 0;
}
