#include "log_var.h"

int main() {
    int x = 13;
    int y = 8;
    LOG("Calculating sum of " << x << " and " << y << ".")
    LOGVAR(x+y)
    return x+y;
}
