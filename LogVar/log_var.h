#ifndef LOG_VAR_H
#define LOG_VAR_H

#ifdef NDEBUG

#define LOG(X)
#define LOGVAR(X)

#else

#include <iostream>
#define LOG(X) \
std::cout << #X << std::endl;
#define LOGVAR(X) \
std::cout << #X << " = " << X << std::endl;

#endif //NDEBUG

#endif //LOG_VAR_H
