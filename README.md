Studies in C++

A project where I keep some small projects that I write to learn about:
- Algorithms;
- Language Features;
- Work Methodologies;
- Tools for Building, Testing, Static Analysis, CI/CD, etc.
