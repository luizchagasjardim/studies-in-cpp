#ifndef PERSON_H
#define PERSON_H

#include "Date.h"
#include <vector>
#include <string>

namespace BirthdayGreeting {

class Person {
public:
    bool operator==(const Person&) const;
    bool operator!=(const Person&) const;
    Person(std::string firstname,
           std::string lastname,
           std::string email,
           Date birthday,
           std::string honorific) :
           firstname(firstname),
           lastname(lastname),
           email(email),
           birthday(birthday),
           honorific(honorific) {}
    std::string get_email() const;
    std::string get_full_name_with_honorific() const;
    std::string get_age_with_ordinal_suffix(const Date& date) const;
    static std::vector<Person> read_csv(const std::string& input_path);
    static std::vector<Person> filter_born_on_this_day(const std::vector<Person>& list_of_people, const Date& date);
private:
std::string firstname;
std::string lastname;
std::string email;
Date birthday;
std::string honorific;
};

} //namespace BirthdayGreeting

#endif //PERSON_H
