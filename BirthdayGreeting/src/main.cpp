#include "BirthdayGreeting.h"
#include "Date.h"
#include <ctime> //TODO: change to chrono 
#include <string>
#include <iostream>
#include <cassert>

int main(int argc, char** argv) {
    assert(argc == 2);
    std::time_t t = std::time(0);
    std::tm* now = std::localtime(&t);
    Date today(now->tm_mday, now->tm_mon + 1, now->tm_year+1900);
    std::cout << BirthdayGreeting::BirthdayGreeting(argv[1], std::string("me@mycompany.com"), today) << "\n";
	return 0;
}
