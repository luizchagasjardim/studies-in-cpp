#ifndef BIRTHDAYGREETING_H
#define BIRTHDAYGREETING_H

#include "Email.h"
#include "Date.h"
#include <string>

namespace BirthdayGreeting {

Email BirthdayGreeting(const std::string& file_input, const std::string& email_address_from, const Date& date);

} //namespace BirthdayGreeting

#endif //BIRTHDAYGREETING_H
