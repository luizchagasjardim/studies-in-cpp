#ifndef EMAIL_H
#define EMAIL_H

#include <string>
#include <ostream>

class Email {
public:
    bool operator==(const Email&) const;
    friend std::ostream& operator<<(std::ostream& os, const Email& e);
    class EmailBuilder {
    public:
        EmailBuilder() {}
        EmailBuilder& set_from(std::string from) {
            builder_from = from;
            return *this;
        }
        EmailBuilder& set_to(std::string to) {
            builder_to = to;
            return *this;
        }
        EmailBuilder& set_cc(std::string cc) {
            builder_cc = cc;
            return *this;
        }
        EmailBuilder& set_cco(std::string cco) {
            builder_cco = cco;
            return *this;
        }
        EmailBuilder& set_subject(std::string subject) {
            builder_subject = subject;
            return *this;
        }
        EmailBuilder& set_body(std::string body) {
            builder_body = body;
            return *this;
        }
        Email build() {
            return Email(builder_from,
                         builder_to,
                         builder_cc,
                         builder_cco,
                         builder_subject,
                         builder_body);
        }
    private:
        std::string builder_from;
        std::string builder_to;
        std::string builder_cc;
        std::string builder_cco;
        std::string builder_subject;
        std::string builder_body;
    };

private:
    Email(std::string from,
          std::string to,
          std::string cc,
          std::string cco,
          std::string subject,
          std::string body) :
          from(from),
          to(to),
          cc(cc),
          cco(cco),
          subject(subject),
          body(body) {}
        const std::string from;
        const std::string to;
        const std::string cc;
        const std::string cco;
        const std::string subject;
        const std::string body;
};

#endif //EMAIL_H
