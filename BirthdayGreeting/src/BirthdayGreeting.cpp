#include "BirthdayGreeting.h"
#include "Person.h"
#include "Email.h"
#include <string>
#include <vector>

namespace BirthdayGreeting {

Email BirthdayGreeting(const std::string& file_input, const std::string& email_address_from, const Date& date) {
    std::vector<Person> every_person = Person::read_csv(file_input);
    std::vector<Person> people_born_on_this_day = Person::filter_born_on_this_day(every_person, date);
    std::string email_body = "";
    std::string email_to = "";
    for (Person& person : people_born_on_this_day) {
        if (email_body != "") {
            email_body += "\n\n";
        }
        email_body += "Congratulations on your " + person.get_age_with_ordinal_suffix(date) + " birthday, " + person.get_full_name_with_honorific() + "!";
        if (email_to != "") {
            email_to += ",";
        }
        email_to += person.get_email();
    }
    Email::EmailBuilder email_builder;
    email_builder.set_from(email_address_from);
    email_builder.set_to(email_to);
    email_builder.set_subject("Happy Birthday!");
    email_builder.set_body(email_body);
    return email_builder.build();
}

} //namespace BirthdayGreeting
