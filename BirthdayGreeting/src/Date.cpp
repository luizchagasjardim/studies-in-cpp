#include "Date.h"
#include <stdexcept>

Date::Date(const int& d, const int& m, const int& y) : day(d), month(m), year(y) {
    if (y == 0) {
        throw std::invalid_argument("There is no year 0");
    }
    if (m < 0 || m > 12) {
        throw std::invalid_argument("Please enter a month between 1 and 12");
    }
    if (d < 0 || d > 31) {
        throw std::invalid_argument("Please enter a day between 1 and 31");
    }
    if (d == 31) {
        if (m == 4 || m == 6 || m == 9 || m == 11) {
            throw std::invalid_argument("Please enter a month between 1 and 12");
        }
    }
    if (m == 2) {
        if (d > 29) {
            throw std::invalid_argument("February can't have more than 29 days");
        }
        if (d == 29 && !isLeapYear(y)) {
            throw std::invalid_argument("This is not a leap year.");
        }
    }
}

Date Date::DateFromString(const std::string& date) {
    return Date::DateFromString(date, '/');
}

Date Date::DateFromString(std::string date, const char& separator) {
    //TODO: use regex instead?
    int date_as_int_array[3];
    size_t separator_pos;
    for (int i = 0; i < 2; i++) {
        if ((separator_pos = date.find(separator)) != std::string::npos) {
            date_as_int_array[i] = stoi(date.substr(0, separator_pos));
            date.erase(0, separator_pos+1);
        } else {
            throw std::invalid_argument("Too few date separators");
        }
    }
    if ((separator_pos = date.find(separator)) != std::string::npos) {
        throw std::invalid_argument("Too many date separators");
    }
    date_as_int_array[2] = stoi(date);
    return Date(date_as_int_array[0], date_as_int_array[1], date_as_int_array[2]);
}

bool Date::operator==(const Date& other) const {
    return year == other.year &&
           month == other.month &&
           day == other.day;
}

bool Date::operator!=(const Date& other) const {
    return !(*this == other);
}

bool Date::operator>(const Date& other) const {
    if (year > other.year) {
        return true;
    } else if (year == other.year) {
        if (month > other.month) {
            return true;
        } else if (month == other.month) {
            return day > other.day;
        }
    }
    return false;
}

bool Date::operator>=(const Date& other) const {
    return (*this > other) || (*this == other);
}

bool Date::operator<(const Date& other) const {
    return !(*this >= other);
}

bool Date::operator<=(const Date& other) const {
    return !(*this > other);
}

bool Date::isSameYearDay(const Date& other) const {
    return month == other.month &&
           day == other.day;
}

bool Date::isSameYear(const Date& other) const {
    return year == other.year;
}

bool Date::isSameMonthDay(const Date& other) const {
    return day == other.day;
}

bool Date::isSameMonth(const Date& other) const {
    return month == other.month;
}

int Date::daysSince(const Date& other) const {
    return toInt() - other.toInt();
}

int Date::monthsSince(const Date& other) const {
    if (*this < other) {
        return -other.monthsSince(*this);
    }
    int ms = 12*yearsSince(other) + month-other.month;
    if (day < other.day) {
        ms--;
    }
    return ms;
}

int Date::yearsSince(const Date& other) const {
    if (*this < other) {
        return -other.yearsSince(*this);
    }
    int ys = year-other.year;
    if (month < other.month) {
        ys--;
    } else {
        if (month == other.month && day < other.day) {
            ys--;
        }
    }
    return ys;
}

bool Date::isInLeapYear() const {
    return isLeapYear(year);
}

int Date::daysInMonth(int m) const {
    switch (m) {
        case 2:
            return isInLeapYear()? 29 : 28;
        case 4:
        case 6:
        case 9:
        case 11:
            return 30;
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            return 31;
    }
    throw std::invalid_argument("The argument should be a month int (1 to 12)");
}

int Date::toInt() const {
    int y = year<0? year : year-1;
    int leapYears = y/4;
    leapYears -= y/100;
    leapYears += y/400;
    int result = y*365 + leapYears;
    for (int i = 1; i < month; i++) {
        result += daysInMonth(i);
    }
    result += day;
    return result;
}

bool Date::isLeapYear(int y) {
    //I don't know if this is valid for negative numbers, but I think no one knows.
    return ((y % 4 == 0) && (y % 100 != 0)) || (y % 400 == 0);
}
