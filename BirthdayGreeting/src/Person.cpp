#include "Person.h"
#include <fstream>
#include <cassert>

namespace BirthdayGreeting {

bool Person::operator!=(const BirthdayGreeting::Person& other) const {
    return !(*this == other);
}

bool Person::operator==(const Person& other) const {
    return firstname == other.firstname &&
           lastname == other.lastname &&
           email == other.email &&
           birthday == other.birthday &&
           honorific == other.honorific;
}

std::string Person::get_email() const {
    return email;
}

std::vector<Person> Person::read_csv(const std::string& input_path) {
    //TODO: move most of this to another class
    //TODO: add try-catch
    std::vector<Person> list_of_people;
    std::ifstream csv(input_path);
    std::string line;
    while (getline(csv, line)) {
        std::vector<std::string> arguments;
        size_t position;
        while ((position = line.find(",")) != std::string::npos) {
            arguments.push_back(line.substr(0, position));
            line.erase(0, position+1);
        }
        arguments.push_back(line);
        assert(arguments.size() == 5);
        list_of_people.emplace_back(arguments.at(0), arguments.at(1), arguments.at(2), Date::DateFromString(arguments.at(3)), arguments.at(4));
    }
    return list_of_people;
}

std::vector<Person> Person::filter_born_on_this_day(const std::vector<Person>& list_of_people, const Date& date) {
    std::vector<Person> filtered_list;
    for (const Person& person : list_of_people) {
        if (person.birthday.isSameYearDay(date)) {
            filtered_list.push_back(person);
        }
    }
    return filtered_list;
}

std::string Person::get_full_name_with_honorific() const {
    return honorific + " " + firstname + " " + lastname;
}

std::string Person::get_age_with_ordinal_suffix(const Date& date) const {
    int age = date.yearsSince(birthday);
    std::string age_with_ordinal_suffix = std::to_string(age);
    switch (age % 10) {
        case 1:
            age_with_ordinal_suffix += "st";
            break;
        case 2:
            age_with_ordinal_suffix += "nd";
            break;
        case 3:
            age_with_ordinal_suffix += "rd";
            break;
        default:
            age_with_ordinal_suffix += "th";
    }
    return age_with_ordinal_suffix;
}



} //namespace BirthdayGreeting
