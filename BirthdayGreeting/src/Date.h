#ifndef DATE_H
#define DATE_H

#include <string>

class Date {
public:
    //constructor and factories
    Date(const int& d, const int& m, const int& y);
    static Date DateFromString(const std::string& date);
    static Date DateFromString(std::string date, const char& separator);
    //spaceship
    bool operator==(const Date& other) const;
    bool operator!=(const Date& other) const;
    bool operator>(const Date& other) const;
    bool operator>=(const Date& other) const;
    bool operator<(const Date& other) const;
    bool operator<=(const Date& other) const;
    //partial equalities
    bool isSameYearDay(const Date& other) const;
    bool isSameYear(const Date& other) const;
    bool isSameMonthDay(const Date& other) const;
    bool isSameMonth(const Date& other) const;
    //difference
    int daysSince(const Date& other) const;
    int monthsSince(const Date& other) const;
    int yearsSince(const Date& other) const;
    //misc
    bool isInLeapYear() const;
private:
    const int day; //TODO: move to its own class?
    const int month; //TODO: move to its own class?
    const int year; //TODO: move to its own class?
    int daysInMonth(int m) const; //TODO: move to month class?
    int toInt() const;
    inline static bool isLeapYear(int y); //TODO: move to year class?
};

#endif //DATE_H
