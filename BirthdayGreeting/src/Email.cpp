#include "Email.h"

bool Email::operator==(const Email& other) const {
    return from == other.from &&
           to == other.to &&
           cc == other.cc &&
           cco == other.cco &&
           subject == other.subject &&
           body == other.body;
}

std::ostream& operator<<(std::ostream& os, const Email& e) {
    os << "From: " << e.from << "\n";
    os << "To: " << e.to << "\n";
    os << "CC: " << e.cc << "\n";
    os << "CCO: " << e.cco << "\n";
    os << "\n";
    os << e.subject << "\n";
    os << "\n";
    os << e.body;
    return os;
}

//TODO: add operator<<
