#include "../src/Person.h"
#include "../src/Date.h"
#include <gtest/gtest.h>
#include <string>
#include <vector>
using BirthdayGreeting::Person;

namespace {

TEST(PersonTest, EqualOperator) {
    Person Person0("Luiz", "Jardim", "jardim@royalty.com", Date(8, 11, 1989), "Your Highness");
    Person Person1("Luiz", "Jardim", "jardim@royalty.com", Date(8, 11, 1989), "Your Highness");
    Person Person2("Luis", "Jardim", "jardim@royalty.com", Date(8, 11, 1989), "Your Highness");
    Person Person3("Luiz", "Jardin", "jardim@royalty.com", Date(8, 11, 1989), "Your Highness");
    Person Person4("Luiz", "Jardim", "jardim@royalty.com.br", Date(8, 11, 1989), "Your Highness");
    Person Person5("Luiz", "Jardim", "jardim@royalty.com", Date(5, 11, 1989), "Your Excellency");
    ASSERT_EQ(Person0, Person1);
    ASSERT_NE(Person1, Person2);
    ASSERT_NE(Person1, Person3);
    ASSERT_NE(Person1, Person4);
    ASSERT_NE(Person1, Person5);
    ASSERT_NE(Person2, Person3);
    ASSERT_NE(Person2, Person4);
    ASSERT_NE(Person3, Person4);
    ASSERT_NE(Person3, Person5);
    ASSERT_NE(Person4, Person5);
}

TEST(PersonTest, ReadCsvTest) {
//     reads a file and outputs a list of people
    const std::string input_filepath = "../files/testfile1.csv";
    std::vector<Person> list_from_csv = Person::read_csv(input_filepath);
    std::vector<Person> expected_list;
    expected_list.emplace_back("Luiz", "Jardim", "jardim@royalty.com", Date(8, 11, 1989), "Your Highness");
    expected_list.emplace_back("Priscilla", "Jardim", "priscilla@royalty.com", Date(2, 4, 1982), "Your Highness");
    expected_list.emplace_back("Serena", "Jardim", "serena@royalty.com", Date(30, 1, 2018), "Your Babyness");
    expected_list.emplace_back("Pluto", "Mause", "plutarco@disnei.com.br", Date(23, 12, 1932), "Your Dogness");
    expected_list.emplace_back("Gamora", "Ramos", "maozinha@danca.io", Date(31, 3, 2015), "Your Barkness");
    expected_list.emplace_back("Kawaii", "Ramos", "sofa_destroyer@metal.pt", Date(12, 7, 1815), "Your Catness");
    EXPECT_EQ(expected_list, list_from_csv);
}

TEST(PersonTest, FilterBornOnThisDayTest) {
//     given a list of people and a date, outputs a new list with people whose birthdays are on that date
    std::vector<Person> given_list;
    given_list.emplace_back("Pluto", "Mause", "plutarco@disnei.com.br", Date(23, 12, 1932), "Your Dogness");
    Person Gamora("Gamora", "Ramos", "maozinha@danca.io", Date(31, 3, 2015), "Your Barkness");
    given_list.push_back(Gamora);
    std::vector<Person> expected_list;
    expected_list.push_back(Gamora);
    std::vector<Person> filtered_list = Person::filter_born_on_this_day(given_list, Date(31, 3, 2015));
    EXPECT_EQ(expected_list, filtered_list);
}

TEST(PersonTest, GetFullNameWithHonorific) {
    EXPECT_EQ("Mr. Frog Rider", Person("Frog", "Rider", "frogrider@lake.eu", Date(14, 8, 1500), "Mr.").get_full_name_with_honorific());
    EXPECT_EQ("Sir Carcassone Stoneage", Person("Carcassone", "Stoneage", "carc.age@euro.uk", Date(14, 8, 1500), "Sir").get_full_name_with_honorific());
    EXPECT_EQ("Your Honor Judge Judge", Person("Judge", "Judge", "judge.judge@iobject.com", Date(14, 8, 1500), "Your Honor").get_full_name_with_honorific());
}

TEST(PersonTest, GetAgeWithOrdinalSuffix) {
    EXPECT_EQ("0th", Person("Frog", "Rider", "frogrider@lake.eu", Date(11, 11, 1641), "Mr.").get_age_with_ordinal_suffix(Date(11, 11, 1641)));
    EXPECT_EQ("1st", Person("Carcassone", "Stoneage", "carc.age@euro.uk", Date(11, 11, 1641), "Sir").get_age_with_ordinal_suffix(Date(1, 12, 1642)));
    EXPECT_EQ("122nd", Person("Judge", "Judge", "judge.judge@iobject.com", Date(11, 11, 1641), "Your Honor").get_age_with_ordinal_suffix(Date(4, 2, 1764)));
}

}  // namespace

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
