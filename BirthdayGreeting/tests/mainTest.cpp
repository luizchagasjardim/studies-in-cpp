#include "../src/BirthdayGreeting.h"
#include "../src/Person.h"
#include "../src/Email.h"
#include "../src/Date.h"
#include <gtest/gtest.h>
#include <string>

namespace {

TEST(BirthdayGreetingTest, FullTest) {
//     reads a file and outputs a birthday greeting
    Date date(8, 11, 2019);
    const std::string input_filepath = "../files/testfile1.csv";
    const std::string from = "me@mycompany.com";
    Email::EmailBuilder email_builder;
    email_builder.set_from(from);
    email_builder.set_to("jardim@royalty.com");
    email_builder.set_subject("Happy Birthday!");
    email_builder.set_body("Congratulations on your 30th birthday, Your Highness Luiz Jardim!");
    const Email email = email_builder.build();
    EXPECT_EQ(email, BirthdayGreeting::BirthdayGreeting(input_filepath, from, date));
}

}  // namespace

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
