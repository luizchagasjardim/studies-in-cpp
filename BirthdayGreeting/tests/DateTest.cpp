#include "../src/Date.h"
#include <gtest/gtest.h>
#include <stdexcept>

namespace {

TEST(ConstructingImpossibleDates, ShouldThrowBecauseOfNegativeDay) {
    EXPECT_THROW(Date(-1, 1, 1995), std::invalid_argument);
}

TEST(ConstructingImpossibleDates, ShouldThrowBecauseOfDayGreaterThan31) {
    EXPECT_THROW(Date(32, 1, 1995), std::invalid_argument);
}

TEST(ConstructingImpossibleDates, ShouldThrowBecauseOfNegativeMonth) {
    EXPECT_THROW(Date(16, -3, 1995), std::invalid_argument);
}

TEST(ConstructingImpossibleDates, ShouldThrowBecauseOfMonthGreaterThan12) {
    EXPECT_THROW(Date(16, 17, 1995), std::invalid_argument);
}

TEST(ConstructingImpossibleDates, ShouldThrowBecauseOfYearZero) {
    EXPECT_THROW(Date(16, 17, 0), std::invalid_argument);
}

TEST(ConstructingImpossibleDates, ShouldThrowBecauseOfFebruary29InNonLeapYear) {
    EXPECT_THROW(Date(29, 2, 1900), std::invalid_argument);
}

TEST(ConstructingImpossibleDates, ShouldThrowBecauseOfDayGreaterThan29InFebruary) {
    EXPECT_THROW(Date(30, 2, 1900), std::invalid_argument);
    EXPECT_THROW(Date(30, 2, 2000), std::invalid_argument);
}

TEST(ConstructingFromString, ShouldBeTheSameAsConstuctingFromThreeInts) {
    EXPECT_EQ(Date::DateFromString("1/1/1845"), Date(1,1,1845));
}

TEST(ConstructingFromString, ShouldNotBeTheSameAsConstuctingFromThreeInts) {
    EXPECT_NE(Date::DateFromString("1/1/1845"), Date(1,1,1846));
}

TEST(ConstructingFromString, ShouldIgnoreLeadingZeroes) {
    EXPECT_EQ(Date::DateFromString("01/01/1845"), Date(1,1,1845));
}

TEST(ConstructingFromString, ShouldConstructWithUserSpecifiedSeparator) {
    EXPECT_EQ(Date::DateFromString("4.2.1311", '.'), Date(4,2,1311));
}

TEST(ConstructingFromString, ShouldNotConstructFromImpossibleDays) {
    EXPECT_THROW(Date::DateFromString("30/2/1900"), std::invalid_argument);
}

TEST(ConstructingFromString, ShouldThrowTooManySeparatorsError) {
    EXPECT_THROW(Date::DateFromString("3/1/7/4"), std::invalid_argument);
}

TEST(ConstructingFromString, ShouldThrowTooFewSeparatorsError) {
    EXPECT_THROW(Date::DateFromString("2/1"), std::invalid_argument);
}

TEST(SpaceshipOperatorTest, ShouldBeEqual) {
    EXPECT_EQ(Date(12, 12, 1912), Date(12, 12, 1912));
}

TEST(SpaceshipOperatorTest, ShoulNotBeEqualBecauseOfDay) {
    EXPECT_NE(Date(12, 12, 1912), Date(13, 12, 1912));
}

TEST(SpaceshipOperatorTest, ShoulNotBeEqualBecauseOfMonth) {
    EXPECT_NE(Date(12, 12, 1912), Date(12, 11, 1912));
}

TEST(SpaceshipOperatorTest, ShoulNotBeEqualBecauseOfYear) {
    EXPECT_NE(Date(12, 12, 1912), Date(12, 12, 783));
}

TEST(SpaceshipOperatorTest, ShoulBeGreaterBecauseOfDay) {
    EXPECT_GT(Date(12, 12, 1912), Date(11, 12, 1912));
}

TEST(SpaceshipOperatorTest, ShoulBeGreaterBecauseOfMonth) {
    EXPECT_GT(Date(12, 12, 1912), Date(23, 11, 1912));
}

TEST(SpaceshipOperatorTest, ShoulBeGreaterBecauseOfYear) {
    EXPECT_GT(Date(12, 9, 1912), Date(23, 12, 1911));
}

TEST(SpaceshipOperatorTest, ShoulBeGreaterOrEqual) {
    EXPECT_GE(Date(12, 9, 1912), Date(23, 12, 1911));
    EXPECT_GE(Date(12, 12, 1912), Date(12, 12, 1912));
}

TEST(SpaceshipOperatorTest, ShoulBeSmallerBecauseOfDay) {
    EXPECT_LT(Date(11, 12, 1912), Date(12, 12, 1912));
}

TEST(SpaceshipOperatorTest, ShoulBeSmallerBecauseOfMonth) {
    EXPECT_LT(Date(26, 10, 1912), Date(23, 11, 1912));
}

TEST(SpaceshipOperatorTest, ShoulBeSmallerBecauseOfYear) {
    EXPECT_LT(Date(12, 9, 1910), Date(23, 12, 1911));
}

TEST(SpaceshipOperatorTest, ShoulBeSmallerOrEqual) {
    EXPECT_LE(Date(12, 9, 1910), Date(23, 12, 1911));
    EXPECT_LE(Date(12, 12, 1912), Date(12, 12, 1912));
}

TEST(isSameYearDayTest, ShouldBeTheSameDayOfTheYear) {
    EXPECT_TRUE(Date(24, 8, 2022).isSameYearDay(Date(24, 8, 2025)));
}

TEST(isSameYearDayTest, ShouldNotBeTheSameDayOfTheYearBecauseOfMonth) {
    EXPECT_FALSE(Date(14, 12, 1700).isSameYearDay(Date(14, 11, 2001)));
}

TEST(isSameYearTest, ShouldBeSameYear) {
    EXPECT_TRUE(Date(7, 6, 512).isSameYear(Date(5, 10, 512)));
}

TEST(isSameYearTest, ShouldNotBeSameYear) {
    EXPECT_FALSE(Date(7, 6, 512).isSameYear(Date(7, 6, 513)));
}

TEST(isSameMonthDayTest, ShouldBeTheSameDayOfTheMonth) {
    EXPECT_TRUE(Date(7, 6, 512).isSameMonthDay(Date(7, 10, 513)));
}

TEST(isSameMonthDayTest, ShouldNotBeTheSameDayOfTheMonth) {
    EXPECT_FALSE(Date(7, 6, 512).isSameMonthDay(Date(8, 10, 512)));
}

TEST(isSameMonthTest, ShouldBeTheSameMonth) {
    EXPECT_TRUE(Date(7, 6, 512).isSameMonth(Date(5, 6, 513)));
}

TEST(isSameMonthTest, ShouldNotBeTheSameMonth) {
    EXPECT_FALSE(Date(7, 6, 512).isSameMonth(Date(7, 10, 512)));
}

TEST(daysSinceTest, ShouldReturnZeroForTheSameDate) {
    EXPECT_EQ(Date(30, 1, 1900).daysSince(Date(30, 1, 1900)), 0);
}

TEST(daysSinceTest, ShouldReturnThatSevenDaysPassed) {
    EXPECT_EQ(Date(30, 1, 1900).daysSince(Date(23, 1, 1900)), 7);
}

TEST(daysSinceTest, ShouldReturnThat290DaysPassed) {
    EXPECT_EQ(Date(30, 12, 1900).daysSince(Date(15, 3, 1900)), 290);
}

TEST(daysSinceTest, ShouldReturnThat423DaysPassed) {
    EXPECT_EQ(Date(30, 1, 1900).daysSince(Date(3, 12, 1898)), 365+31+27);
}

TEST(daysSinceTest, ShouldReturnThatItIs1020DaysUntilThen) {
    EXPECT_EQ(Date(30, 1, 1900).daysSince(Date(3, 12, 1902)), -(365*2 + 4 + (28+31+30+31+30+31+31+30+31+30)));
}

TEST(monthsSinceTest, ShouldReturnZeroForTheSameMonth) {
    EXPECT_EQ(Date(30, 1, 1900).monthsSince(Date(12, 1, 1900)), 0);
}

TEST(monthsSinceTest, ShouldReturnThatItIsTenMonthsUntilThen) {
    EXPECT_EQ(Date(30, 1, 1900).monthsSince(Date(23, 12, 1900)), -10);
}

TEST(monthsSinceTest, ShouldReturnThatNineMonthsPassed) {
    EXPECT_EQ(Date(30, 12, 1900).monthsSince(Date(15, 3, 1900)), 9);
}

TEST(yearsSinceTest, ShouldReturnZeroForTheSameYear) {
    EXPECT_EQ(Date(30, 1, 1900).yearsSince(Date(30, 1, 1900)), 0);
}

TEST(yearsSinceTest, ShouldReturnThatSevenYearsPassed) {
    EXPECT_EQ(Date(30, 1, 1900).yearsSince(Date(23, 1, 1893)), 7);
}

TEST(isInLeapYear, ShouldBeALeapYearBecauseItIsAMultipleOf400) {
    EXPECT_TRUE(Date(1, 1, 2000).isInLeapYear());
}

TEST(isInLeapYear, ShouldNotBeALeapYearBecauseItIsNotAMultipleOf4) {
    EXPECT_FALSE(Date(4, 10, 2001).isInLeapYear());
}

TEST(isInLeapYear, ShouldBeALeapYearBecauseItIsAMultipleOf4ButNotOf100) {
    EXPECT_TRUE(Date(5, 7, 2004).isInLeapYear());
}

TEST(isInLeapYear, ShouldNotBeALeapYearBecauseItIsAMultipleOf100ButNotOf400) {
    EXPECT_FALSE(Date(28, 2, 2100).isInLeapYear());
}

}  // namespace

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

