#include "../src/Budget.h"
#include "../src/Euro.h"
#include <gtest/gtest.h>
#include <stdexcept>
#include <string>
#include <filesystem>

using namespace BudgetApplication;

namespace {

TEST(CreateBudgetTest, ShouldCreateAnEmptyBudget) {
    Budget b = Budget::create_empty();
    EXPECT_TRUE(b.isEmptyBudget()); //TODO: Does this method really have to exist?
}

//Test Fixture
class AddAndRemoveCategoryTest : public ::testing::Test {
protected:
    Budget budget = Budget::create_empty();
    const std::string category_name_1 = "groceries";
    const std::string category_name_2 = "tv-phone-internet package";
    const std::string category_name_3 = "electric bill";
    const std::string category_name_4 = "salary";
    void SetUp() override {
        budget.addCategory(category_name_1);
        budget.addCategory(category_name_2, 30_e);
        budget.addCategory(category_name_3, 20_e);
        budget.addCategory(category_name_4, 5000_e, CATEGORY_TYPE::INCOME);
    }
};

TEST_F(AddAndRemoveCategoryTest, ShouldHaveADefaultCategoryCreatedInTheFixtureSetUp) {
    EXPECT_TRUE(budget.hasCategory(category_name_1));
}

TEST_F(AddAndRemoveCategoryTest, ShouldThrowForCreatingAnExistingCategory) {
    EXPECT_THROW(budget.addCategory(category_name_1), std::runtime_error);
}

TEST_F(AddAndRemoveCategoryTest, Category1ShouldHaveDefaultBudgetedValue) {
    ASSERT_EQ(budget.get_budgeted_amount(category_name_1), 0_e);
}

TEST_F(AddAndRemoveCategoryTest, Category1ShouldHaveDefaultSpentValue) {
    ASSERT_EQ(budget.get_total_spent_by_category(category_name_1), 0_e);
}

TEST_F(AddAndRemoveCategoryTest, Category1ShouldBeOutcomeByDefault) {
    ASSERT_FALSE(budget.is_income(category_name_1));
}

TEST_F(AddAndRemoveCategoryTest, ShouldRemoveCategory) {
    budget.removeCategory(category_name_1);
    EXPECT_FALSE(budget.hasCategory(category_name_1));
}

TEST_F(AddAndRemoveCategoryTest, ShouldThrowForRemovingNonExistingCategory) {
    EXPECT_THROW({
        budget.removeCategory(category_name_1);
        budget.removeCategory(category_name_1);
    }, std::runtime_error);
}

TEST_F(AddAndRemoveCategoryTest, Category2ShouldHaveTheSpecifiedBudgetedAmount) {
    ASSERT_EQ(budget.get_budgeted_amount(category_name_2), 30_e);
}

TEST_F(AddAndRemoveCategoryTest, Category2ShouldHaveTheSpecifiedSpentAmount) {
    ASSERT_EQ(budget.get_total_spent_by_category(category_name_2), 0_e);
}

TEST_F(AddAndRemoveCategoryTest, Category2ShouldBeOutcomeByDefault) {
    ASSERT_FALSE(budget.is_income(category_name_2));
}

TEST_F(AddAndRemoveCategoryTest, Category3ShouldHaveTheSpecifiedBudgetedAmount) {
    ASSERT_EQ(budget.get_budgeted_amount(category_name_3), 20_e);
}

TEST_F(AddAndRemoveCategoryTest, Category3ShouldHaveTheSpecifiedSpentAmount) {
    ASSERT_EQ(budget.get_total_spent_by_category(category_name_3), 0_e);
}

TEST_F(AddAndRemoveCategoryTest, Category3ShouldBeOutcomeByDefault) {
    ASSERT_FALSE
    (budget.is_income(category_name_3));
}

TEST_F(AddAndRemoveCategoryTest, Category4ShouldHaveTheSpecifiedBudgetedAmount) {
    ASSERT_EQ(budget.get_budgeted_amount(category_name_4), 5000_e);
}

TEST_F(AddAndRemoveCategoryTest, Category4ShouldHaveTheSpecifiedReceivedAmount) {
    ASSERT_EQ(budget.get_total_spent_by_category(category_name_4), 0_e);
}

TEST_F(AddAndRemoveCategoryTest, Category4ShouldBeIncome) {
    ASSERT_TRUE
    (budget.is_income(category_name_4));
}

//Test Fixture
class BudgetForATimePeriodTest : public ::testing::Test {
protected:
    Budget budget = Budget::create_empty();
    const std::string category_name_1 = "groceries";
    const std::string category_name_2 = "tv-phone-internet package";
    const std::string category_name_3 = "electric bill";
    const std::string category_name_4 = "salary";
    void SetUp() override {
        budget.addCategory(category_name_1);
        budget.addCategory(category_name_2, 30_e);
        budget.addCategory(category_name_3, 20_e);
        budget.addCategory(category_name_4, 5000_e, CATEGORY_TYPE::INCOME);
    }
};

TEST_F(BudgetForATimePeriodTest, ShouldChangeCurrentBudgetForCategory1) {
    budget.budget(category_name_1, 250_e);
    ASSERT_EQ(budget.get_budgeted_amount(category_name_1), 250_e);
}

TEST_F(BudgetForATimePeriodTest, ShouldChangeCurrentBudgetForCategory2) {
    budget.budget(category_name_2, 42.5_e);
    ASSERT_EQ(budget.get_budgeted_amount(category_name_2), 42.5_e);
}

TEST_F(BudgetForATimePeriodTest, ShouldChangeCurrentBudgetForCategory3) {
    budget.budget(category_name_3, 10_e);
    ASSERT_EQ(budget.get_budgeted_amount(category_name_3), 10_e);
}

TEST_F(BudgetForATimePeriodTest, ShouldChangeCurrentBudgetForCategory4) {
    budget.budget(category_name_4, 6000_e);
    ASSERT_EQ(budget.get_budgeted_amount(category_name_4), 6000_e);
}

//Test Fixture
class MakeTransactionTest : public ::testing::Test {
protected:
    Budget budget = Budget::create_empty();
    const std::string category_name_1 = "groceries";
    const std::string category_name_4 = "salary";
    const std::string person_name_1 = "luiz";
    const std::string person_name_2 = "priscilla";
    const std::string establishment_name_1 = "continente";
    const std::string establishment_name_2 = "jumbo";
    const std::string establishment_name_3 = "nice company";
    void SetUp() override {
        budget.addCategory(category_name_1);
        budget.addCategory(category_name_4, 5000_e, CATEGORY_TYPE::INCOME);
    }
};

TEST_F(MakeTransactionTest, ShouldSpendNothingOnGroceries) {
    budget.make_transaction(person_name_1, establishment_name_1, 0_e, category_name_1);
    ASSERT_EQ(budget.get_total_spent_by_category(category_name_1), 0_e);
}

TEST_F(MakeTransactionTest, ShouldSpendPositiveAmountOnGroceries) {
    budget.make_transaction(person_name_2, establishment_name_1, 10_e, category_name_1);
    ASSERT_EQ(budget.get_total_spent_by_category(category_name_1), 10_e);
    
}

TEST_F(MakeTransactionTest, ShouldSpendNegativeAmountOnGroceries) {
    //For example, you could have won a coupon
    budget.make_transaction(person_name_2, establishment_name_2, -5_e, category_name_1);
    ASSERT_EQ(budget.get_total_spent_by_category(category_name_1), -5_e);
}

TEST_F(MakeTransactionTest, ShouldGetPaid) {
    budget.make_transaction(person_name_2, establishment_name_3, 2000_e, category_name_4);
    ASSERT_EQ(budget.get_total_spent_by_category(category_name_4), 2000_e);
}

//Test Fixture
class SaveAndLoadTest : public ::testing::Test {
protected:
    Budget budget = Budget::create_empty();
    const std::string category_name_1 = "groceries";
    const std::string category_name_2 = "tv-phone-internet package";
    const std::string category_name_3 = "electric bill";
    const std::string category_name_4 = "salary";
    const std::string person_name_1 = "luiz";
    const std::string person_name_2 = "priscilla";
    const std::string establishment_name_1 = "continente";
    const std::string establishment_name_2 = "jumbo";
    const std::string establishment_name_3 = "nice company";
    const std::filesystem::path file_path = "../files/test_file.json";
    Budget loaded_budget = Budget::create_empty();
    void SetUp() override {
        budget.addCategory(category_name_1);
        budget.addCategory(category_name_2, 30_e);
        budget.addCategory(category_name_3, 20_e);
        budget.addCategory(category_name_4, 5000_e, CATEGORY_TYPE::INCOME);
        budget.make_transaction(person_name_1, establishment_name_1, 0_e, category_name_1);
        budget.make_transaction(person_name_2, establishment_name_1, 10_e, category_name_1);
        budget.make_transaction(person_name_2, establishment_name_2, -5_e, category_name_1);
        budget.make_transaction(person_name_2, establishment_name_3, 2000_e, category_name_4);
        budget.save(file_path);
        loaded_budget = Budget::load(file_path);
    }
};

TEST_F(SaveAndLoadTest, ShouldHaveLoadedSomething) {
    EXPECT_FALSE(loaded_budget.isEmptyBudget());
}

TEST_F(SaveAndLoadTest, ShouldLoadWithTheSameCategoriesAsSaved) {
    EXPECT_TRUE(loaded_budget.hasCategory(category_name_1));
    EXPECT_TRUE(loaded_budget.hasCategory(category_name_2));
    EXPECT_TRUE(loaded_budget.hasCategory(category_name_3));
    EXPECT_TRUE(loaded_budget.hasCategory(category_name_4));
}

TEST_F(SaveAndLoadTest, ShouldLoadWithTheSameBudgetedAmountsAsSaved) {
    EXPECT_EQ(loaded_budget.get_budgeted_amount(category_name_1), 0_e);
    EXPECT_EQ(loaded_budget.get_budgeted_amount(category_name_2), 30_e);
    EXPECT_EQ(loaded_budget.get_budgeted_amount(category_name_3), 20_e);
    EXPECT_EQ(loaded_budget.get_budgeted_amount(category_name_4), 5000_e);
}

TEST_F(SaveAndLoadTest, ShouldLoadWithTheSameCategoryTypesAsSaved) {
    EXPECT_EQ(loaded_budget.get_category_type(category_name_1), CATEGORY_TYPE::OUTCOME);
    EXPECT_EQ(loaded_budget.get_category_type(category_name_2), CATEGORY_TYPE::OUTCOME);
    EXPECT_EQ(loaded_budget.get_category_type(category_name_3), CATEGORY_TYPE::OUTCOME);
    EXPECT_EQ(loaded_budget.get_category_type(category_name_4), CATEGORY_TYPE::INCOME);
}

TEST_F(SaveAndLoadTest, ShouldLoadWithTheSameTransactionLists) {
    EXPECT_EQ(loaded_budget.get_transaction_list().size(), budget.get_transaction_list().size());
    EXPECT_EQ(loaded_budget.get_transaction_list(category_name_1).size(), budget.get_transaction_list(category_name_1).size());
    EXPECT_EQ(loaded_budget.get_transaction_list(category_name_2).size(), budget.get_transaction_list(category_name_2).size());
    EXPECT_EQ(loaded_budget.get_transaction_list(category_name_3).size(), budget.get_transaction_list(category_name_3).size());
    EXPECT_EQ(loaded_budget.get_transaction_list(category_name_4).size(), budget.get_transaction_list(category_name_4).size());
}

}  // namespace

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}