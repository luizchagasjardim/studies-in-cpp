#include "../src/Euro.h"
#include <gtest/gtest.h>

namespace {

TEST(LiteralTest, ShouldCreateInstanceFromFloat) {
    EXPECT_NO_FATAL_FAILURE({
        12.4_e;
    });
}

TEST(LiteralTest, ShouldCreateInstanceFromInt) {
    EXPECT_NO_FATAL_FAILURE({
        65_e;
    });
}

TEST(InsertionOperatorTest, ShouldOutputCorrectlyToTheOStream1) {
    std::ostringstream actual;
    actual << 0.0_e;
    std::string expected("€0");
    EXPECT_EQ(actual.str(), expected);
}

TEST(InsertionOperatorTest, ShouldOutputCorrectlyToTheOStream2) {
    std::ostringstream actual;
    actual << 124_e;
    std::string expected("€124");
    EXPECT_EQ(actual.str(), expected);
}

TEST(InsertionOperatorTest, ShouldOutputCorrectlyToTheOStream3) {
    std::ostringstream actual;
    actual << 5.43123_e;
    std::string expected("€5,43");
    EXPECT_EQ(actual.str(), expected);
}

TEST(InsertionOperatorTest, ShouldOutputCorrectlyToTheOStream4) {
    std::ostringstream actual;
    actual << -5_e;
    std::string expected("-€5");
    EXPECT_EQ(actual.str(), expected);
}

} //namespace

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
