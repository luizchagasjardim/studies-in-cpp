#include "CommandLineInterpreter.h"

namespace BudgetApplication {

void CommandLineInterpreter::start_budget_application(int argc, char** argv) {
    
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help,h", "Produce help message.")
        ("load,l", po::value<std::string>(&load_path), "Path to a saved budget.")
        ("auto_save,s", po::bool_switch(&auto_save)->default_value(false), "Auto save after every operation.")
    ;
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << '\n';
        exit(0);
    }

    if (vm.count("load")) {
        std::cout << "Loading from " << load_path << "..." << '\n';
        //TODO: catch
        budget = Budget::load(load_path);
    }

    std::cout << budget << '\n';
}

bool CommandLineInterpreter::execute_command() {
    
    //read command line
    std::string cmd;
    getline(std::cin, cmd);
    if (command_line_history.empty() || command_line_history.back() != cmd) {
        if (cmd != "show_command_line_history") {
            command_line_history.push_back(cmd);
        }
    }
    std::vector<std::string> args;
    boost::split(args, cmd, boost::is_any_of(" "));

    std::string function = args.at(0);
    args.erase(args.begin());
    if (function == "add_category") {
        add_category(args);
    } else if (function == "remove_category") {
        remove_category(args);
    } else if (function == "budget_amount") {
        budget_amount(args);
    } else if (function == "make_transaction") {
        make_transaction(args);
    } else if (function == "show_transaction_list") {
        show_transaction_list(args);
    } else if (function == "show_command_line_history") {
        show_command_line_history(args);
    } else if (function == "exit") {
        return false;
    }

    if (auto_save) {
        std::cout << "Saving...";
        budget.save(load_path);
        std::cout << "Saved!" << '\n';
    }
    
    return true;

}

void CommandLineInterpreter::add_category(std::vector<std::string> args) {
    std::string name;
    long double budgeted;
    bool is_income;
    bool dont_print_budget; //TODO: change to print_budget with default_true. for some reason it doesn't work
    bool print_transations;
    po::options_description options("Add a new category to the current budget");
    options.add_options()
        ("help,h", "Produce help message.")
        ("name,n", po::value<std::string>(&name), "Category name")
        ("budgeted,b", po::value<long double>(&budgeted)->default_value(0), "Amount reserved for this category. May be changed later through the 'budget' command.")
        ("income,i", po::bool_switch(&is_income)->default_value(false), "Income category, e.g., salary. If ommitted, the category is considered outcome (expense).")
        ("no-print", po::bool_switch(&dont_print_budget)->default_value(false), "Do not print budget.")
        ("print-transactions,t", po::bool_switch(&print_transations)->default_value(false), "Print transaction list.")
    ;
    po::positional_options_description positional;
    positional.add("name", -1);
    po::variables_map vm;
    po::store(po::command_line_parser(args).options(options).positional(positional).run(), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << options << '\n';
        return;
    }

    if (!vm.count("name")) {
        std::cout << "Please include a name." << std::endl;
        return;
    } else {
        //TODO: catch every possible exception
        budget.addCategory(name, Euro(budgeted), is_income? CATEGORY_TYPE::INCOME : CATEGORY_TYPE::OUTCOME);
    }

    if (!dont_print_budget) {
        std::cout << budget << '\n';
    }
    
    if (print_transations) {
        print_transactions();
    }

}

void CommandLineInterpreter::remove_category(std::vector<std::string> args) {
    std::string name;
    bool dont_print_budget; //TODO: change to print_budget with default_true. for some reason it doesn't work
    bool print_transations;
    po::options_description options("Remove a category from the current budget");
    options.add_options()
        ("help,h", "Produce help message.")
        ("name,n", po::value<std::string>(&name), "Category name")
        ("no-print", po::bool_switch(&dont_print_budget)->default_value(false), "Do not print budget.")
        ("print-transactions,t", po::bool_switch(&print_transations)->default_value(false), "Print transaction list.")
    ;
    po::positional_options_description positional;
    positional.add("name", -1);
    po::variables_map vm;
    po::store(po::command_line_parser(args).options(options).positional(positional).run(), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << options << '\n';
        return;
    }

    if (!vm.count("name")) {
        std::cout << "Please include a name." << std::endl;
        return;
    } else {
        //TODO: catch every possible exception
        budget.removeCategory(name);;
    }

    if (!dont_print_budget) {
        std::cout << budget << '\n';
    }
    
    if (print_transations) {
        print_transactions();
    }

}

void CommandLineInterpreter::budget_amount(std::vector<std::string> args) {

    std::string name;
    long double budget_amount;
    bool dont_print_budget; //TODO: change to print_budget with default_true. for some reason it doesn't work
    bool print_transations;
    po::options_description options("Change a category's budgeted amount.");
    options.add_options()
        ("help,h", "Produce help message.")
        ("name,n", po::value<std::string>(&name), "Category name")
        ("budget,b", po::value<long double>(&budget_amount)->default_value(0), "Amount reserved for this category. May be changed later through the 'budget' command.")
        ("no-print", po::bool_switch(&dont_print_budget)->default_value(false), "Do not print budget.")
        ("print-transactions,t", po::bool_switch(&print_transations)->default_value(false), "Print transaction list.")
    ;
    po::positional_options_description positional;
    positional.add("name", 1);
    positional.add("budget", -1);
    po::variables_map vm;
    po::store(po::command_line_parser(args).options(options).positional(positional).run(), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << options << '\n';
        return;
    }

    if (!vm.count("name")) {
        std::cout << "Please include a name." << std::endl;
        return;
    }

    if (!vm.count("budget")) {
        std::cout << "Please include a budget value." << std::endl;
        return;
    }

    //TODO: catch every possible exception
    budget.budget(name, Euro(budget_amount));

    if (!dont_print_budget) {
        std::cout << budget << '\n';
    }
    
    if (print_transations) {
        print_transactions();
    }

}

void CommandLineInterpreter::make_transaction(std::vector<std::string> args) {

    std::string person_name;
    std::string establishment_name;
    long double amount;
    std::string category_name;
    bool dont_print_budget; //TODO: change to print_budget with default_true. for some reason it doesn't work
    bool print_transations;
    po::options_description options("Register a transaction.");
    options.add_options()
        ("help,h", "Produce help message.")
        ("person,p", po::value<std::string>(&person_name), "Name of the person making the transaction.")
        ("establishment,e", po::value<std::string>(&establishment_name), "Name of the establishment accepting the transaction.")
        ("amount,a", po::value<long double>(&amount)->default_value(0), "Value of transaction.")
        ("category,c", po::value<std::string>(&category_name), "Category name")
        ("no-print", po::bool_switch(&dont_print_budget)->default_value(false), "Do not print budget.")
        ("print-transactions,t", po::bool_switch(&print_transations)->default_value(false), "Print transaction list.")
    ;
    po::positional_options_description positional;
    positional.add("budget", -1);
    po::variables_map vm;
    po::store(po::command_line_parser(args).options(options).positional(positional).run(), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << options << '\n';
        return;
    }

    if (!vm.count("person")) {
        std::cout << "Please include the name of the person making the transaction." << std::endl;
        return;
    }

    if (!vm.count("establishment")) {
        std::cout << "Please include the name of the establishment accepting the transaction." << std::endl;
        return;
    }

    if (!vm.count("amount")) {
        std::cout << "Please include the amount of the transaction." << std::endl;
        return;
    }

    if (!vm.count("category")) {
        std::cout << "Please include the category of the transaction." << std::endl;
        return;
    }

    //TODO: catch every possible exception
    budget.make_transaction(person_name, establishment_name, Euro(amount), category_name);

    if (!dont_print_budget) {
        std::cout << budget << '\n';
    }
    
    if (print_transations) {
        print_transactions();
    }
}

void CommandLineInterpreter::show_transaction_list(std::vector<std::string> args) {
    po::options_description options("Show the list of all transactions.");
    options.add_options()
        ("help,h", "Produce help message.")
    ;
    po::variables_map vm;
    po::store(po::command_line_parser(args).options(options).run(), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << options << '\n';
        return;
    }
    
    print_transactions();
}

void CommandLineInterpreter::show_command_line_history(std::vector<std::string> args) {
    int max;
    int size = command_line_history.size();
    po::options_description options("Show history of commands");
    options.add_options()
        ("help,h", "Produce help message.")
        ("max,M", po::value<int>(&max)->default_value(size), "Maximum number of lines.");
    po::variables_map vm;
    po::store(po::command_line_parser(args).options(options).run(), vm);
    po::notify(vm);

    for (int i = size - max; i < max; i++) {
        std::cout << command_line_history[i] << '\n';
    }
}

void CommandLineInterpreter::print_transactions() {
    for (Transaction& t : budget.get_transaction_list()) {
        std::cout << t.person_name << " -> " << t.establishment_name << " : " << t.amount << '\n';
    }
}

}
