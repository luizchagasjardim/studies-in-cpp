#ifndef EURO_H
#define EURO_H

#include <ostream>

//TODO: the same for every currency (using templates? macros?)
//TODO: overload a bunch of other operators
class Euro {
public:
    explicit Euro(long double amount) : amount(amount) {};
    static Euro read_euro_string(std::string);
    friend Euro operator""_e(long double);
    friend Euro operator""_e(unsigned long long);
    friend std::ostream& operator<<(std::ostream&, Euro);
    std::string to_string();
    bool operator==(const Euro&) const;
    Euro operator-() const;
    Euro& operator+=(const Euro&);
private:
    long double amount;
    constexpr static char separator = ',';
    int get_whole();
    int get_cents();
};

Euro operator""_e(long double);
Euro operator""_e(unsigned long long);
std::ostream& operator<<(std::ostream&, Euro);


#endif //EURO_H
