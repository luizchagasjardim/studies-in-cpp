#ifndef BUDGETFILEMANAGER_H
#define BUDGETFILEMANAGER_H

#include "Budget.h"
#include <filesystem>
#include <string>

namespace BudgetApplication {

/**
 * Basically a rapidjson wrapper.
 */
class BudgetFileManager {
public:
    static BudgetFileManager create(const std::filesystem::path&);
    Budget load();
    void save(const Budget&);
private:
    BudgetFileManager(const std::filesystem::path& full_path, const std::filesystem::path& folder, const std::filesystem::path& file);
    const std::string full_path;
    const std::string folder;
    const std::string file;
    //rapid json wrapping functions
    Budget read_json(const std::string& json);
    std::string write_json(const Budget& b);
};

} //BudgetApplication

#endif //BUDGETFILEMANAGER_H
