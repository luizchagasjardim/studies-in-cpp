#include "Category.h"
#include <algorithm>

namespace BudgetApplication {

Category::Category(std::string name, Euro budgeted, CATEGORY_TYPE type) : name(name), budgeted(budgeted), type(type) {
    
}

Euro Category::get_budgeted_amount() const {
    return budgeted;
}

Euro Category::get_spent_or_received_amount() const {
    return get_total_spent();
}

bool Category::is_income() const {
    return type == CATEGORY_TYPE::INCOME;
}

void Category::budget(Euro amount) {
    budgeted = amount;
}

void Category::make_transaction(std::string person_name, std::string establishment_name, Euro amount) {
    Transactions.emplace_back(person_name, establishment_name, amount);
}

Euro Category::get_total_spent() const {
    Euro total = 0_e;
    for (const Transaction& t : Transactions) {
        total += t.amount;
    }
    return total;
}

CATEGORY_TYPE Category::get_type() {
    return type;
}

std::vector<Transaction> Category::get_transactions() {
    return Transactions;
}

} //namespace BudgetApplication
