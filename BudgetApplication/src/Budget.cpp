#include "Budget.h"
#include "BudgetPrinter.h"
#include "BudgetFileManager.h"

namespace BudgetApplication {

Budget Budget::create_empty() {
    Budget b;
    return b;
}

bool Budget::hasCategory(const std::string& name) const {
    return Categories.find(name) != Categories.end();
}
    
void Budget::addCategory(std::string name, Euro budgeted, CATEGORY_TYPE type)
{
    if (!hasCategory(name)) {
        Categories.emplace(name, Category(name, budgeted, type));
    } else {
        throw std::runtime_error("There is already an category with that name!");
    }
}

void Budget::removeCategory(std::string name) {
    if (hasCategory(name)) {
        Categories.erase(name);
    } else {
        throw std::runtime_error("There is no category with that name!");
    }
}

CATEGORY_TYPE Budget::get_category_type(std::string name) {
    return get_category(name).get_type();
}

bool Budget::isEmptyBudget() const {
    return Categories.empty() && People.empty() && Establishments.empty();
}

Euro Budget::get_budgeted_amount(std::string name) const {
    return get_category(name).get_budgeted_amount();
}

Euro Budget::get_spent_or_received_amount(std::string name) const {
    return get_category(name).get_spent_or_received_amount();
}

bool Budget::is_income(std::string name) const {
    return get_category(name).is_income();
}

void Budget::budget(std::string name, Euro amount) {
    return get_category(name).budget(amount);
}

void Budget::make_transaction(std::string person_name, std::string establishment_name, Euro amount, std::string category_name) {
    People.insert(person_name);
    Establishments.insert(establishment_name);
    get_category(category_name).make_transaction(person_name, establishment_name, amount);
}

std::vector<Transaction> Budget::get_transaction_list() {
    std::vector<Transaction> all_transactions;
    for (auto& category : Categories) {
        std::vector<Transaction> category_transactions = category.second.get_transactions();
        all_transactions.insert(all_transactions.end(), category_transactions.begin(), category_transactions.end());
    }
    return all_transactions;
}

std::vector<Transaction> Budget::get_transaction_list(std::string category_name) {
    return get_category(category_name).get_transactions();
}

Euro Budget::get_total_spent_by_category(std::string name) {
    return get_category(name).get_total_spent();
}

Budget::Budget() {}

Category& Budget::get_category(std::string name) {
    if (hasCategory(name)) {
        return Categories.at(name);
    } else {
        throw std::runtime_error("There is no category with that name!");
    }
}

const Category& Budget::get_category(std::string name) const {
    if (hasCategory(name)) {
        return Categories.at(name);
    } else {
        throw std::runtime_error("There is no category with that name!");
    }
}

void Budget::save(const std::filesystem::path& path) const {
    BudgetFileManager saver = BudgetFileManager::create(path);
    saver.save(*this);
}

Budget Budget::load(const std::filesystem::path& path) {
    BudgetFileManager loader = BudgetFileManager::create(path);
    return loader.load();
}

} //namespace BudgetApplication

std::ostream& operator<<(std::ostream& os, const BudgetApplication::Budget& b) {
    os << BudgetApplication::BudgetPrinter::print(b);
    return os;
}
