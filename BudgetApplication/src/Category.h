#ifndef CATEGORY_H
#define CATEGORY_H

#include "Euro.h"
#include "Transaction.h"
#include <string>
#include <vector>

namespace BudgetApplication {

class BudgetFileManager;

enum class CATEGORY_TYPE {
    INCOME,
    OUTCOME
};

class Category {
public:
    Category(std::string name, Euro budgeted, CATEGORY_TYPE type = CATEGORY_TYPE::OUTCOME);
    Euro get_budgeted_amount() const;
    Euro get_spent_or_received_amount() const;
    bool is_income() const;
    void budget(Euro amount);
    void make_transaction(std::string person_name, std::string establishment_name, Euro amount);
    Euro get_total_spent() const;
//TODO     Euro get_remaining_budget();
    CATEGORY_TYPE get_type();
    std::vector<Transaction> get_transactions();
    friend BudgetFileManager; //remove this and implement getters 
private:
    const std::string name;
    Euro budgeted;
    const CATEGORY_TYPE type;
    std::vector<Transaction> Transactions; //I thought a lot about whether this put this here or in Budget, but I haven't come to a conclusion
};

} //namespace BudgetApplication

#endif //CATEGORY_H
