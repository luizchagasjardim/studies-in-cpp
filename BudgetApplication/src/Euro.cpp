#include "Euro.h"
#include <sstream>

Euro Euro::read_euro_string(std::string in) {
    bool negative = (in.front() == '-');
    if (negative) {
        in = in.substr(4, in.length()-1);
    } else {
        in = in.substr(3, in.length()-1);
    }
    Euro ret = Euro(std::stold(in));
    return negative? -ret : ret;
}

Euro operator""_e(long double amount) {
    return Euro(amount);
}

Euro operator""_e(unsigned long long amount) {
    return Euro(amount);
}

std::ostream& operator<<(std::ostream& os, Euro e) {
    os << e.to_string();
    return os;
}

std::string Euro::to_string() {
    //TODO: re-write this whole darn thing
    std::stringstream os;
    if (amount < 0) {
        amount = -amount;
        os << '-';
    }
    os << "€" << get_whole();
    auto cents = get_cents();
    if (cents == 0) {
        return os.str();
    } else if (cents < 10) {
        os << '0';
    }
    os << Euro::separator << cents;
    return os.str();
}

bool Euro::operator==(const Euro& other) const {
    return amount == other.amount;
}

Euro Euro::operator-() const {
    return Euro(-amount);
}

Euro& Euro::operator+=(const Euro& other) {
    amount += other.amount;
    return *this;
}

int Euro::get_whole() {
    return (int) amount;
}

int Euro::get_cents() {
    return (int) (100*(amount - get_whole()));
}
