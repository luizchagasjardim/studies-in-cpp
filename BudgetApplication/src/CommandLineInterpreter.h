#ifndef COMMAND_LINE_INTERPRETER_H
#define COMMAND_LINE_INTERPRETER_H

#include "Budget.h"
#include <boost/program_options.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <iostream>
#include <string>
#include <vector>

namespace po = boost::program_options;

namespace BudgetApplication {

//Starts the program creating or loading a budget
class CommandLineInterpreter {
public:
    void start_budget_application(int argc, char** argv);
    bool execute_command();
    void add_category(std::vector<std::string>);
    void remove_category(std::vector<std::string>);
    void budget_amount(std::vector<std::string>);
    void make_transaction(std::vector<std::string>);
    void show_transaction_list(std::vector<std::string>);
    void show_command_line_history(std::vector<std::string>);
private:
    void print_transactions();
    std::string load_path;
    bool auto_save;
    Budget budget = Budget::create_empty();
    std::vector<std::string> command_line_history;
};

}

#endif //COMMAND_LINE_INTERPRETER_H
