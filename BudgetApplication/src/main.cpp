#include "Budget.h"
#include "CommandLineInterpreter.h"
#include <iostream>

using namespace BudgetApplication;

int main(int argc, char* argv[]) {

    CommandLineInterpreter cli; //not to be confused with command line interface lol

    cli.start_budget_application(argc, argv);

    while(cli.execute_command()) {}

    return 0;
}
