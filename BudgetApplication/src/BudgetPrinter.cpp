#include "BudgetPrinter.h"
#include "Budget.h"
#include <sstream>

namespace BudgetApplication {

//TODO: rewrite this file
    
std::string BudgetPrinter::print(Budget b) {
    std::stringstream os;
    os << print_border();
    os << "\n";
    os << "## " << to_max_n_chars("Category") << " # " << to_max_n_chars("Budgeted") << " # " << to_max_n_chars("Received/Spent") << " ##\n";
    for (const auto& pair : b.Categories) {
        os << "## " << to_max_n_chars(pair.first) << " # " << to_max_n_chars(pair.second.get_budgeted_amount().to_string()) << " # " << to_max_n_chars(pair.second.get_spent_or_received_amount().to_string()) << " ##\n";
    }
    os << print_border();
    return os.str();
}

BudgetPrinter::BudgetPrinter() {
    
}

unsigned int BudgetPrinter::max_n_chars() {
    return 12;
}

unsigned int BudgetPrinter::utf8_length(const char* str) {
    unsigned int length = 0;
    while (*str) length += (*str++ & 0xc0) != 0x80;
    return length;
}

unsigned int BudgetPrinter::utf8_length(std::string str) {
    return utf8_length(str.c_str());
}

std::string BudgetPrinter::to_max_n_chars(std::string str) {
    if (utf8_length(str) > max_n_chars()) {
        str = str.substr(0, max_n_chars()-3) + "...";
    }
    while (utf8_length(str) < max_n_chars()) {
        str += " ";
    }
    return str;    
}

std::string BudgetPrinter::print_border() {
    std::stringstream os;
    for (unsigned int i = 0; i < 12 + 3*max_n_chars(); i++) {
        os << '#';
    }
    return os.str();
}

}
