#include "BudgetFileManager.h"
#include "Category.h"
#include "Transaction.h"
#include <rapidjson/document.h>
#include <rapidjson/prettywriter.h>
#include <fstream>

namespace fs = std::filesystem;

namespace BudgetApplication {

BudgetFileManager BudgetFileManager::create(const fs::path& path) {

    fs::path folder = path.parent_path();
    fs::create_directories(folder);

    fs::path file = path.stem();
    file += ".json";

    fs::path full_path = folder/file;

    return BudgetFileManager(full_path.string(), folder.string(), file.string());
}

Budget BudgetFileManager::load() {
    std::ifstream load_stream;
    load_stream.open(full_path);
    if (!load_stream.is_open()) throw std::runtime_error("could not open " + full_path);
    std::stringstream stringstream;
    stringstream << load_stream.rdbuf();
    Budget b = read_json(stringstream.str());
    load_stream.close();
    return b;
}

void BudgetFileManager::save(const Budget& b) {
    std::ofstream save_stream;
    save_stream.open(full_path);
    if (!save_stream.is_open()) throw std::runtime_error("could not open " + full_path);
    save_stream << write_json(b);
    save_stream.close();
}

BudgetFileManager::BudgetFileManager(const fs::path& full_path, const fs::path& folder, const fs::path& file) : full_path(full_path), folder(folder), file(file) {
    
}

Budget BudgetFileManager::read_json(const std::string& json) {
    Budget budget = Budget::create_empty();
    rapidjson::Document doc;
    rapidjson::ParseResult ok = doc.Parse(json.c_str()); //TODO: I don't even remember why this variable is here. but it's unused. Check this later.
    if (!ok) {
        throw std::runtime_error("not ok... ???");
    }
    
    //from here onwards it's all hacky and stuff but i just want to be done with this
    try {
        auto it = doc.FindMember("categories");
        if (it != doc.MemberEnd()) {
            for (auto& value : it->value.GetArray()) {
                rapidjson::Value category = value.GetObject();
                std::string category_name = category.FindMember("name")->value.GetString();
                std::string budgeted_str = category.FindMember("budgeted")->value.GetString();
                Euro budgeted = Euro::read_euro_string(budgeted_str);
                CATEGORY_TYPE type;
                std::string type_str = category.FindMember("type")->value.GetString();
                if (type_str == "INCOME") {
                    type = CATEGORY_TYPE::INCOME;
                } else if (type_str == "OUTCOME") {
                    type = CATEGORY_TYPE::OUTCOME;
                } else {
                    throw std::invalid_argument("invalid type for category " + category_name);
                }
                budget.addCategory(category_name, budgeted, type);
                auto transaction_it = category.FindMember("transactions");
                if (transaction_it != category.MemberEnd()) {
                    for (auto& transaction_element : transaction_it->value.GetArray()) {
                        rapidjson::Value transaction = transaction_element.GetObject();
                        std::string person_name = transaction.FindMember("person")->value.GetString();
                        std::string establishment_name = transaction.FindMember("establishment")->value.GetString();
                        std::string amount_str = transaction.FindMember("amount")->value.GetString();
                        Euro amount = Euro::read_euro_string(amount_str);
                        budget.make_transaction(person_name, establishment_name, amount, category_name);
                    }
                }
            }
        }
    } catch (std::invalid_argument& e) {
        throw;
    } catch (...) {
        throw std::runtime_error("Invalid categories array in json file.");
    }

    return budget;
}

std::string BudgetFileManager::write_json(const Budget& b) {
    rapidjson::Document doc;
    auto& allocator = doc.GetAllocator();
    doc.SetObject();

    if (!b.Categories.empty()) {
        rapidjson::Value categories_array(rapidjson::kArrayType);
        for (auto& category : b.Categories) {
            rapidjson::Value category_obj(rapidjson::kObjectType);

            //TODO: make this a function?
            rapidjson::Value name;
            name.SetString(category.first.c_str(), allocator);
            category_obj.AddMember("name", name, allocator);
            
            rapidjson::Value budgeted;
            budgeted.SetString(category.second.get_budgeted_amount().to_string().c_str(), allocator);
            category_obj.AddMember("budgeted", budgeted, allocator);
            
            rapidjson::Value type;
            type.SetString(category.second.is_income()? "INCOME" : "OUTCOME", allocator);
            category_obj.AddMember("type", type, allocator);

            if (!category.second.Transactions.empty()) {
                rapidjson::Value transactions_array(rapidjson::kArrayType);
                for (auto& transaction : category.second.Transactions) {
                    rapidjson::Value transaction_obj(rapidjson::kObjectType);

                    rapidjson::Value person;
                    person.SetString(transaction.person_name.c_str(), allocator);
                    transaction_obj.AddMember("person", person, allocator);
                    
                    rapidjson::Value establishment;
                    establishment.SetString(transaction.establishment_name.c_str(), allocator);
                    transaction_obj.AddMember("establishment", establishment, allocator);
                    
                    rapidjson::Value amount;
                    Euro e = transaction.amount;
                    amount.SetString(e.to_string().c_str(), allocator);
                    transaction_obj.AddMember("amount", amount, allocator);

                    transactions_array.PushBack(transaction_obj, allocator);
                }
                category_obj.AddMember("transactions", transactions_array, allocator);
            }
            
            categories_array.PushBack(category_obj, allocator);
        }
        doc.AddMember("categories", categories_array, allocator);
    }

    rapidjson::StringBuffer buffer;
    rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buffer);
    doc.Accept(writer);
    return buffer.GetString();
}

} //BudgetApplication
