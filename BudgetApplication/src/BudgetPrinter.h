#ifndef BUDGETPRINTER_H
#define BUDGETPRINTER_H

#include <string>

namespace BudgetApplication {

class Budget;

class BudgetPrinter {
public:
    static std::string print(Budget);
private:
    BudgetPrinter();
    static unsigned int max_n_chars();
    static unsigned int utf8_length(const char* str);
    static unsigned int utf8_length(std::string);
    static std::string to_max_n_chars(std::string);
    static std::string print_border();
};

}

#endif //BUDGETPRINTER_H
