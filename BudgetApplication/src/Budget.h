#ifndef BUDGET_H
#define BUDGET_H

#include "Category.h"
#include "Euro.h"
#include "Transaction.h"
#include <string>
#include <map>
#include <set>
#include <sstream>
#include <vector>
#include <filesystem>

namespace BudgetApplication {
class Budget;
class BudgetPrinter;
class BudgetFileManager;
}

std::ostream& operator<<(std::ostream&, const BudgetApplication::Budget&);

namespace BudgetApplication {

class Budget {
public:
    [[nodiscard]] static Budget create_empty();
    bool hasCategory(const std::string& name) const;
    void addCategory(std::string name, Euro budgeted = 0_e, CATEGORY_TYPE type = CATEGORY_TYPE::OUTCOME);
    void removeCategory(std::string name);
    CATEGORY_TYPE get_category_type(std::string name);
    bool isEmptyBudget() const;
    Euro get_budgeted_amount(std::string name) const;
    Euro get_spent_or_received_amount(std::string name) const;
    bool is_income(std::string name) const;
    void budget(std::string name, Euro amount);
    void make_transaction(std::string person_name, std::string establishment_name, Euro amount, std::string category_name);
    std::vector<Transaction> get_transaction_list();
    std::vector<Transaction> get_transaction_list(std::string category_name);
    Euro get_total_spent_by_category(std::string name);
    friend std::ostream& (::operator<<)(std::ostream&, const Budget&);
    friend BudgetPrinter;
    void save(const std::filesystem::path& path) const;
    [[nodiscard]] static Budget load(const std::filesystem::path& path);
    friend BudgetFileManager;
private:
    Budget();
    Category& get_category(std::string name);
    const Category& get_category(std::string name) const;
    std::map<std::string, Category> Categories;
    std::set<std::string> People; //list of all people ever mentioned
    std::set<std::string> Establishments; //list of all establishments ever mentioned
};

} //namespace BudgetApplication

#endif //BUDGET_H
