#ifndef TRANSACTION_H
#define TRANSACTION_H

#include "Euro.h"

struct Transaction {
    Transaction(std::string person_name, std::string establishment_name, Euro amount) : person_name(person_name), establishment_name(establishment_name), amount(amount) {};
    std::string person_name;
    std::string establishment_name;
    Euro amount;
};

#endif //TRANSACTION_H
